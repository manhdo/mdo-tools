from bottle import route, run, template, post

import pymongo
import sys

connection = pymongo.Connection("mongodb://localhost",safe=True)
db = connection.m101 #database m101
people = db.people


#people.insert(person)


@route('/hello/:name')
def index(name='World'):
    return template('<b>Hello {{name}}</b>!', name=name)


@post("/favorite_fruit")
def favorite_fruit():
	fruit = bottle.request.forms.get("fruit")


	#redirect
	bottle.redirect("/shot_fruit")



#run(host='localhost', port=8080)